const express = require('express');
const findTask = require('../middleware/findTask');
const userAuth = require('../middleware/userAuth');

const Task = require('../models/Task');



const router = express.Router();

router.post('/', userAuth, async (req, res) => {
    try {
        const task = new Task(req.body);
        task.user = req.user._id
        await task.save();
        return res.send(task)
    } catch (e) {
        return res.status(400).send(e)
    }
});

router.get('/', userAuth, async (req, res) => {
    try {
        const criteria = {};
        criteria.user = req.user._id
        const tasks = await Task.find(criteria);
        res.send(tasks)
    } catch (e) {
        return res.status(400).send(e)
    }
})

router.get('/:id', [userAuth, findTask], async (req, res) => {
    try {
        res.send(req.task)
    } catch (e) {
        return res.status(400).send(e)
    }
});

router.put('/:id', [userAuth, findTask], async (req, res) => {
    try {
        const task = req.task
        task.overwrite(req.body)
        task.user = req.user._id;
        await task.save();
        res.send({ message: 'task edited', task })
    } catch (e) {
        return res.status(400).send(e)
    }
});

router.delete('/:id', [userAuth, findTask], async (req, res) => {
    try {
        const task = req.task
        await Task.deleteOne({ _id: task._id })
        res.send({ message: 'task deleted' })
    } catch (e) {
        return res.status(400).send(e)
    }
})

module.exports = router