const express = require('express');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const mongoose = require('mongoose');

const users = require('./app/users');
const tasks = require('./app/tasks');

const app = express();
app.use(express.json());
app.use(cors());


const port = 8000;


app.use('/users', users);
app.use('/tasks', tasks)


const run = async () => {
    await mongoose.connect('mongodb://localhost/todo', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
    exitHook(async callback => {
        console.log('Disconnecting')
        await mongoose.disconnect();
        callback();
    });
}

run().catch(console.error)