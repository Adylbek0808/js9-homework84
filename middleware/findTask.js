const Task = require('../models/Task');

const findTask = async (req, res, next) => {
    try {
        const criteria = {}
        criteria.user = req.user._id
        criteria._id = req.params.id
        const task = await Task.findOne(criteria)
        if (!task) {
            return res.send({ error: 'You have no such task' })
        }
        req.task = task;
        next();
    } catch (e) {
        res.send(e)
    }
}

module.exports = findTask;